%define name demo-app
%define version 0.1.0
%define release 0
%define myuser root
%define mygroup root
%define prefix_dir /u01/app
%define install_dir %{prefix_dir}/demo-app
%define demoapp_logdir %{_localstatedir}/log/demo-app
%define debug_package %{nil}
%define _binaries_in_noarch_packages_terminate_build   0
%define _unpackaged_files_terminate_build 0

Name:   %{name}
Version: %{version}
Release: %{release}
Summary: Demo App
Group:  System Environment/Base
License: GPL
Source0:  ~/rpmbuild/SOURCES/%{name}-%{version}.tar.gz
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root
AutoReqProv: no

%description
Demo App is a REST based API for clients to query their data.

%prep
%setup -q -c %{name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{install_dir}
cp -R . %{buildroot}/%{install_dir}
%{__install} -p -d -m 0755 %{buildroot}%{demoapp_logdir}


%post
touch %{demoapp_logdir}/access.log
touch %{demoapp_logdir}/error.log
touch %{demoapp_logdir}/demo_app_debug.log
touch %{demoapp_logdir}/demo_app_info.log
touch %{demoapp_logdir}/demo_app_warn.log


%clean
[ "%{buildroot}" != '/' ] && rm -rf %{buildroot}


%files
%defattr(-, root, root)
%{prefix_dir}
%{demoapp_logdir}


%changelog
* Thu Nov 2121019 John Hogarty <hogihung@gmail.com>
- Version 0.0.1, 0
- Initial Release 
