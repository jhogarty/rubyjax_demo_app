#! /bin/sh

# Wait for DB services
sh .docker/wait-for-services.sh

# Prepare DB 
sh .docker/prepare-db.sh

# Start the Prometheus Exporter
nohup bundle exec prometheus_exporter --prefix demo_app_ &

# Ensure container stays running 
# tail -f README.md
tail -f logs/access.log
